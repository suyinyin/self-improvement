"""
ADT LinearList
List(self)
is_empty(self)
len(self)
travel(self)              print all elements in list
prepend(self)            insert an element as first position
append(self)
insert(self)
del_first(self)
del_last(self)
del(self, i)
search(self, elem)
forall(self, op)          do op for every element in this list
"""
class Lnode:
    'creat the list node'
    def __init__(self, data, pnext = None):
        self.data = data
        self.pnext = pnext

class LinkedList:
    def __init__(self):
        self.head = None
        self.length = 0

    def initList(self, data):
        self.head = Lnode(data[0])
        p = self.head
        for i in data[1:]:
            node = Lnode(i)
            p.pnext = node
            p = p.pnext

    def is_empty(self):
        if self.head == None:
            return True
        else:
            return False

    def lengthg(self):
        if self.is_empty():
            print "this is empty list"
        else:
            p = self.head
            while p != None:
                self.length += 1
                p = p.pnext
            return self.length

    def prepend(self, element):
        node = Lnode(element)
        p = self.head
        if self.is_empty():
            self.head = node
        else:
            node.pnext = p
            p = node
            self.head = p

    def travelist(self):
        if self.is_empty():
            print "this is empty list"
        p = self.head
        while p != None:
            print p.data,
            p = p.pnext

    def appendlist(self, element):
        new_node = Lnode(element)
        if self.is_empty():
            self.head = new_node
        p = self.head
        while p.pnext is not None:
            p = p.pnext
        p.pnext = new_node

    def insert(self, element, index):
        new_node = Lnode(element)
        if self.is_empty():
            self.head = new_node
        i = 0
        p = self.head
        while i < index-1:
            p = p.pnext
            i += 1
        new_node.pnext = p.pnext
        p.pnext = new_node

    def del_first(self):
        if self.is_empty():
            print 'This is an empty list'
        p = self.head
        p = p.pnext
        self.head = p

    def del_last(self):
        if self.is_empty():
            print 'This is an empty list'
        p = self.head
        i = 0
        lenth = self.lengthg()
        while i < int(lenth)-2 :
            p = p.pnext
            i += 1
        print i
        p.pnext = None

    def delElem(self, index):
        if self.is_empty():
            print 'This is an empty list'
        i = 0
        p = self.head
        while i < index-1:
            p = p.pnext
            i += 1
        p.pnext = p.pnext.pnext

    def search(self, elem):
        if self.is_empty():
            print 'This is an empty list'
        i = 0
        p = self.head
        index = []
        while p is not None:
            if p.data == elem:
                index.append(i)
            p = p.pnext
            i+=1
        return index

list1 = LinkedList()
list1.initList([1,3,2,3,7,3])
list1.prepend(4)
list1.appendlist(6)
list1.insert(10,3)
list1.del_first()
list1.del_last()
list1.delElem(2)
list1.travelist()
index = list1.search(3)
print index


