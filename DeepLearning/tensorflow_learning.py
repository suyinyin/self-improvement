import numpy as np
import tensorflow as tf
import pandas as pd

x_data =  np.random.rand(100).astype(np.float32)   #   set up x_data set using random
y_data = x_data * 0.1 + 0.3

#create tensorflow  structure start #

weights = tf.Variable(tf.random_uniform([1],-0.1, 1.0))     # 1 denotes the dimension of variable in [-1.1]
baises =  tf.Variable(tf.zero([1]))   # internalize 0

y = weights * x_data + baises
loss = tf.reduce_mean(tf.square(y - y_data))           #compute cost function

optimizer = tf.train.GradientDescentOptimizer(0.5)      #set optimize machine  using GDO 0.5 is learning rate
train = optimizer.minimize(loss)

init = tf.initialize_ all_variables()                  # initialize compute structure
#create tensorflow structure 

sess =tf.Session()
see.run(init)                          # this step is very important

for step in range(201):
	sess.run(train)
	if step % 20 == 0:
		 print(step, sess.run(Weights), sess.run(biases))
		 

#### Session interaction control

import tensorflow as tf

matrix1 = tf.constrant([[3, 3]])
matrix2 = tf.constrant([[1],[2]])

product = tf.matmul(matrix1,matrix2)     # matrix multiply using tf

# method1

sess = tf.Session()
result = sess.run(product)
print(result)
sess.close()



#method2
with tf.Session() as sess:         #open Session using with
	reslut = sess.run(product)
	print(reslut2)

	
	
# define variable in tf

import tensorflow as tf

state = tf.Variable(0, name = 'counter')

one = tf.constant(1)

new_value = tf.add(state, one)
update = tf.assign(state, new_value)

# if you set some variables , you must  initialize 

init = tf.initialize_all_variables()

with tf.Session() as sess:
	sess.run(init)                        #important
	for _ in range(3):
	sess.run(undate)
	print(sess.run(state))
	
	
	
	
#  placeholder()

import tensorflow as tf

input1 = tf.placeholder(tf.float32)               #placeholder is related to feed_dict()
input2 = tf.placeholder(tf.float32)

output = tf.mul(input1, input2)

with tf.Session() as sess:
	print(sess.run(output, feef_dict = {input1: [7.0], input2: [2.0]}))




# activate function :


# how to add layer

import as tensorflow as tf

# define the layer function which hold 4 parameters, and default activation function is Noe
def add_layer(inputs, in_size, out_size, activation_function = None):
	Weights = tf.variable(tf.random_normal([in_size, out_size]))     # do not set all parameters as 0
	biases = tf.Variable(tf.zero([1,out_size]) + 0.1)
	Wx_plus_b = tf.matmul(input,weights) + baises
	
	if activation_function is None:
		outputs = Wx_plus_b
	else:
		outputs =activation_function(Wx_plus_b)
	return outputs
 	

	
# construct neural networks:
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt


def add_layer(inputs, in_size, out_size, activation_function = None):
	Weights = tf.variable(tf.random_normal([in_size, out_size]))     # do not set all parameters as 0
	biases = tf.Variable(tf.zero([1,out_size]) + 0.1)
	Wx_plus_b = tf.matmul(input,weights) + baises
	
	if activation_function is None:
		outputs = Wx_plus_b
	else:
		outputs =activation_function(Wx_plus_b)
	return outputs

xs = tf.placeholder([None, 1],dtype = float32)               #placeholder is related to feed_dict()
ys = tf.placeholder([None, 1],dtype = float32)	

x_data = np.linspace(-1,1,300, dtype=np.float32)[:, np.newaxis]
noise = np.random.normal(0, 0.05, xs.shape).astype(np.float32)
y_data = np.square(xs) - 0.5 + noise

# define hidden layer:

l1 = add_layer(x_data, 1, 10, activation_function = tf.nn.relu)

prediction = add_layer(1l, 10, 1, activation_function = None)

loss = tf.reduce_mean(tf.reduce_sum(tf.squre(y_data - prediction),   #二者差的平方求和再取平均
						reduction_indices  = [1]))


train_step = tf.train.GradientDescentOptimizer(0.1).minimize(loss)  #对误差进行分析，通过梯度减小误差


init = tf.initialize_all_variables()
sess =Session()
sess.run(init)

fig = plt.figure()
ax = fig.add_subplots(1, 1, 1)
ax.scatter(x_data, y_data)
plt.ion()   #连续的plot
plt.show()



for i in range (1000):
	sess.run(train_step, feed_dict = {xs: x_data, ys: y_data})
	  if i % 50 == 0:
        # to see the step improvement
       # print(sess.run(loss, feed_dict={xs: x_data, ys: y_data}))
	           # to visualize the result and improvement
        try:
            ax.lines.remove(lines[0])
        except Exception:
            pass
		prediction_value = sess.run(prediction, feed_dict{xs:x_data})
		line = ax.plot(x_data, prediction_value, 'r-', lw = 5)
		ax.lines.remove(lines[0])
		plt.pause(.1)


		
#how to visulize the NN using tensorboard

#使用with tf.name_scope('inputs')可以将xs和ys包含进来，形成一个大的图层，
#图层的名字就是with tf.name_scope()方法里的参数。
with tf.name_scope('input')     #建立图层
	xs = tf.placeholder([None, 1],dtype = float32, name = 'x_input')               #placeholder is related to feed_dict()
	ys = tf.placeholder([None, 1],dtype = float32, name = 'y_input')	
writer = tf.train.SummaryWriter("logs/", sess.graph)     

# 在terminal下面使用：
# tensorboard --logdir logs 就可以看到师徒框架了。
#http://0.0.0.0:6006 网址打不开的朋友们, 请使用 http://localhost:6006, 大多数朋友都是这个问题.





















