import numpy as np
import pandas as pd
import tensorflow as tf



#情况1：当没有设置图级seed和操作级seed时，生成的随机数是随机的 
#情况2：当设置操作级seed时，生成的随机数是同一组随机数，没有设置操作级seed的操作，
#生成的随机数是随机的
#如果图层面的随机种子没有被设置，但是操作层面的随机种子被设置了，那么被设置随机种
#子的操作层将有确定的唯一种子，其他操作层不具有唯一种子。
#(int)可以任意设置
tf.set_random_seed(1)

# input the data set mnist
from tensorflow.examples.tutorials.mnist import input_data

# if there is no datasets included in this computer, this command is used to download data from INTERNET
mnist = input_data.read_data_sets('MINIST_data', one_hot = True)    



# hyperparameters

lr = 0.001  #learning rate
training_iters = 100000        #train step
batch_size = 128
n_inputs = 28              #Mnist pixels 28*28
n_steps = 28 
n_hidden_units = 128               # neurons in hidden layer
n_class = 10                        #number 0~9

# x_image, y placeholder

x = tf.placeholder(tf.float32, [None, n_shapes, n_inputs])
y = tf.placeholder(tf.float32, [None, n_class])

#define weights and biases
weights = {'in' : tf.Variable(tf.random_normal([n_inputs, n_hidden_units])),    #28X128
		  'out': tf.Variable(tf.random_normal([n_hidden_units, n_class]))}     #128X10
biases = {'in' : tf.Variable(tf.constant(0.1, shape = [n_hidden_units, ])),    #128X1
		  'out': tf.Variable(tf.constant([n_class, ]))}                        #10X1

		  
		  
		  
#define RNN
def RNN(X, weights, biases):
	# hidden layer for input to cell (input_gate)
	#X(128batch, 28 steps, 28 inputs)
	#X = tf.reshape(128*28,  28 inputs)
	X = tf.reshape(X, [-1, n_input])
	X_in = tf.matmul(X, weights['in']) + biases['in']
	X_in = tf.reshape(X_in, [-1, n_steps, n_hidden_units])
	
	#cell
	#state_is_tuple = True means that there are two sorts of states, on is c_state that memory while the other is m_state added to main state by signal of forget gate.
	lstm_cell = tf.nn.rnn_cell.BasicalLSTMCell(n_hidden_units, forget_bias = 1.0, state_is_tuple = True)
	_init_state = lstm_cell.zero_state(batch_size, tf.float32)
	
	#tf.nn.dynamic_rnn is recommend by tensorflow, states is the last state at the last layer
	#  time_major = False    Do not choose first element of X_in
	output, states = tf.nn.dynamic_rnn(lstm_cell, X_in, initial_state = _init_state, time_major = False)
	
	
	#hidden layer for output as the final results
	results = tf.matmul(states[1], weights['out']) + biases['out']
	
	pred = RNN(x, weights, biases)
	cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y))
	train_op = tf.train.AdamOptimizer(lr).minimize(cost)	  
		  
	correct_pred = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
	accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# init= tf.initialize_all_variables() # tf 马上就要废弃这种写法
# 替换成下面的写法:
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    step = 0
    while step * batch_size < training_iters:
        batch_xs, batch_ys = mnist.train.next_batch(batch_size)
        batch_xs = batch_xs.reshape([batch_size, n_steps, n_inputs])
        sess.run([train_op], feed_dict={
            x: batch_xs,
            y: batch_ys,
        })
        if step % 20 == 0:
            print(sess.run(accuracy, feed_dict={
            x: batch_xs,
            y: batch_ys,
        }))
        step += 1
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  





