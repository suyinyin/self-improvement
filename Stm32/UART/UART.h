#ifndef INCLUDE_UART_H_
#define INCLUDE_UART_H_
/*------------------------------------------------------------------------------------------
 * Created on: 21th May, 2021
 * Author: Yinyin SU
 * E-mail: yinyinsu1991@gmail.com
 *------------------------------------------------------------------------------------------
 * Functions descriptions:
 * void Key_Init();
 * u8 Key_Scan(u8 mode);
 *-------------------------------------------------------------------------------------------
 * */

// Include necessary libraries.
#include "stm32f7xx_hal.h"
#include "stm32f7xx_hal_uart.h"
#include "stdio.h"

#if SYSTEM_SUPPORT_OS
#include "includes.h"					//os
#endif

// Define macro variables
#define EN_USART1_RX 			1
#define RXBUFFERSIZE   			1
#define USART_REC_LEN  			200
typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;

uint8_t aRxBuffer[RXBUFFERSIZE];
uint8_t USART_RX_BUF[USART_REC_LEN];
uint16_t USART_RX_STA;
UART_HandleTypeDef UART1_Handler;

void uart_init(uint32_t baud_rate);

void USART1_IRQHandler(void);

#endif
