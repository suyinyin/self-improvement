#include "Key.h"



// Initializing all keys
void Key_Init(){

	// define GPIO initializer
	GPIO_InitTypeDef GPIO_Inititure;

	// Initialize the IO port clock
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();

	// set PA0
	GPIO_Inititure.Pin = GPIO_PIN_0;
	GPIO_Inititure.Mode = GPIO_MODE_INPUT;
	GPIO_Inititure.Pull = GPIO_PULLDOWN;
	GPIO_Inititure.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(GPIOA, &GPIO_Inititure);

	// set PC13
	GPIO_Inititure.Pin = GPIO_PIN_13;
	GPIO_Inititure.Mode = GPIO_MODE_INPUT;
	GPIO_Inititure.Pull = GPIO_PULLUP;
	GPIO_Inititure.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(GPIOC, &GPIO_Inititure);

	// set PH2 and PH3
	GPIO_Inititure.Pin = GPIO_PIN_2 | GPIO_PIN_3;
	GPIO_Inititure.Mode = GPIO_MODE_INPUT;
	GPIO_Inititure.Pull = GPIO_PULLUP;
	GPIO_Inititure.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(GPIOH, &GPIO_Inititure);
}


// Scanning all keys
u8 Key_Scan(u8 mode){

	static u8 key_up = 1;


	if(mode == 1)
		key_up = 1;
	if(key_up && (KEY0==0 || KEY1==0 || KEY2==0 ||WK_UP==1)){

		HAL_Delay(10);
		key_up = 0;
		if(KEY0 == 0) return KEY0_PRES;
		else if(KEY1 == 0) return KEY1_PRES;
		else if(KEY2 == 0) return KEY2_PRES;
		else if(WK_UP == 1) return WKUP_PRES;
	}
	else if(KEY0==1 && KEY1==1 && KEY2==1 && WK_UP==0)
		key_up = 1;

	return 0;
}
























