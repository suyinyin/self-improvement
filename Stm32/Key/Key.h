#ifndef INCLUDE_KEY_H_
#define INCLUDE_KEY_H_
/*------------------------------------------------------------------------------------------
 * Created on: 20th May, 2021
 * Author: Yinyin SU
 * E-mail: yinyinsu1991@gmail.com
 *------------------------------------------------------------------------------------------
 * Functions descriptions:
 * void Key_Init();
 * u8 Key_Scan(u8 mode);
 *-------------------------------------------------------------------------------------------
 * */

// Include necessary libraries.
#include "stm32f7xx_hal.h"

// Macro definitions for some useful variable type that is used frequently.
typedef int32_t  s32;
typedef int16_t s16;
typedef int8_t  s8;
typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;


#define KEY0        HAL_GPIO_ReadPin(GPIOH,GPIO_PIN_3)  //KEY0 PH3
#define KEY1        HAL_GPIO_ReadPin(GPIOH,GPIO_PIN_2)  //KEY1 PH2
#define KEY2        HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_13) //KEY2 PC13
#define WK_UP       HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0)  //WKUP PA0

#define KEY0_PRES 	1  	//KEY0
#define KEY1_PRES	2	//KEY1
#define KEY2_PRES	3	//KEY2
#define WKUP_PRES   4	//WKUP


// Initializing all keys
void Key_Init();

// Scanning all keys
u8 Key_Scan(u8 mode);


#endif /* INCLUDE_KEY_H_ */
