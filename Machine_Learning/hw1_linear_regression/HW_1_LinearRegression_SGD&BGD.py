import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
import latex
import time
sys.path.append(r'/home/python/venv/lib/home_work_ML/introduction_to_ml_with_python-master')
import mglearn
x, y = mglearn.datasets.make_wave(n_samples=300)
# x = np.linspace(-10, 10, 100)
# y = x*1 + 0.1
# print(x.shape)
# print(y)

# define linear regression function
start = time.clock()

# you can choose GD method by make SGD = True or False, True is SGD while False is BGD
def LinearRegression(x_data, y_label, learningrate, SGD = True ):   
    # x_data is input feature
    # y_label is label in dataset
    # n is the n-th polynomial
    # learningrate is the learning rate when compute the weights
    weights = 0
    biase = 0
    epsilon = 1e-8
    j = 0
    if SGD == True:
        while j < 2000:
            y_prediction = (weights * x_data + biase)
            i = 0
            for i in np.arange(len(y_label)):
                weights1 = weights + learningrate * (y_label[i] - y_prediction[i, 0]) * x_data[i, 0]
                biase1 = biase + learningrate * (y_label[i] - y_prediction[i, 0]) * 1
                weights = weights1
                biase = biase1
            j += 1
        print("Gradient Descent method is SGD")
        return weights, biase
    else:
        while True:
            y_prediction = (weights * x_data + biase).T
            weights1 = weights + learningrate * np.sum(np.dot(y_label - y_prediction, x_data))
            biase1 = biase + learningrate * np.sum(np.dot(y_label - y_prediction,1))
            if (abs(weights1 - weights) < epsilon) & (abs(biase - biase1) < epsilon):        #epsilon is convergence condition
                print("Gradient Descent method is BGD")
                return weights, biase
                break
            weights = weights1
            biase = biase1


plt.scatter(x, y, marker='o', color='b')
plt.title(r'linear regression $f(x) = \omega ^ \top x +b$')
plt.xlabel('input features')
plt.ylabel('linear label')
weights, biase = LinearRegression(x, y, 0.0001, SGD = False)
y1 = weights*x +biase
plt.plot(x, y1,'-r')
plt.grid(True)
end = time.clock()
# mglearn.plots.plot_linear_regression_wave()
print('weights = %f') %(weights)
print('biase= %f') %(biase)
print('total time = %fs') %(end - start)
plt.show()
