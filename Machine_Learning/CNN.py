import tensorflow as tf
import numpy as np
import pandas as pd

'''
classification using tensorflow with CNN 
'''
# input the data set mnist
from tensorflow.examples.tutorials.mnist import input_data

# if there is no datasets included in this computer, this command is used to download data from INTERNET
mnist = input_data.read_data_sets('MINIST_data', one_hot = True)  


# define weight_variable and biases in order to convolution
def weight_variable(shape):
	
	#tf.truncated_normal(shape, mean, stddev) :shape表示生成张量的维度，mean是均值，stddev是标准差。
	initial = tf.truncated_normal(shape, stddev = 0.1)
	return tf.Variable(initial)

def bias_variable(shape):
	initial = tf.constant(0.1, shape = shape)
	return = tf.Variable(initial)
	
def conv2d(x, W):

	# strides[1, x_movement, y_movement, 1]
	#padding equal to 'SAME' or 'valid' mean the dimension of feature map matrix is equal to inputs
	return tf.nn.conv2d(x, w, strides = [1, 1, 1, 1], padding = 'SAME')

def max_pooling_2x2(x):
	# ksize = [1, row_dimension, column_dimension, 1]
	return tf.nn.max_pool(x, ksize = [1, 2, 2, 1], strides = [1,2,2,1]) 

def add_layer(inputs, in_size, out_size, activation_function = None):
	# add one more layer and return the output of this layer
	Weights = tf.Variable(tf.random_normal([in_size, out_size]))
	biases = tf.Variable(tf.zeros([1, out_size])+0.1)
	Wx_plus_b = tf.matmul(inputs, Weights) + biases
	if activation_function is None:
		outputs = Wx_plus_b
	else:
		outputs = activation_function(Wx_plus_b)
	return outputs

def compute_accuracy(v_xs, v_ys):
	global prediction
	
	 # y_pre is vector whose dimension is 10*1, but its elements are not 0 or 1,
	 # every element denotes the probability that number occurs 
	 
	y_pre = sess.run(prediction, feed_dict = {xs: v_xs})   
	
	# argmax() denotes that finds the index of max element as row (0: column, 1: row )
	correct_prediction = tf.equal(tf.argmax(y_pre, 1), tf.argmax(v_ys, 1))
	
	# cast() in this example denotes that convert the type of prediction to float32
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))   
	result = sess.run(accuracy, feed_dict = {xs: v_xs, ys: v_ys})
	return result
# define placeholder for inputs to network

xs = tf.placeholder(tf.float32, [None, 784])  #28*28

ys = tf.placeholder(tf.float32, [None, 10])  # every number can be denoted in 10 classification

keep_prob = tf.placeholder(tf.float32)

# tf.shape(image, [-1: not consider the image samples, 28X28: pixels, 1: the image is black-white rather than RGB])
x_image = tf.reshape(xs, [-1, 28, 28, 1])

# conv1 layer:
# patch 5X5, black-white channel:1, outsize 32 feature map
W_conv1 = weight_variable([5,5,1,32])
b_conv1 = bias_variable([32])
# convolution and nonlinear
h_conv1=tf.nn.relu(conv2d(x_image,W_conv1)+b_conv1)   #output size 28X28X32
h_pool1 = max_pooling_2x2(h_conv1)                    #output size 14X14X32 

# conv2 layer:
W_conv2 = weight_variable([5,5,1,32])
b_conv2 = bias_variable([64])
h_conv2=tf.nn.relu(conv2d(h_pool1,W_conv2)+b_conv2)   #output size 14X14X64
h_pool2 = max_pooling_2x2(h_conv2)                    #output size 7X7X64

# fully function layer1:
W_fc1 = weight_variable([7*7*64,1024])    # input size: 7*7*64  output size:  1024

b_fc1 = weight_variable([1024])


h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)


# fully function layer1:
W_fc2 = weight_variable([1024,10])    # input size: 7*7*64  output size:  1024

b_fc2 = weight_variable([10])

prediction = tf.nn.softmax(tf.matmul(h_fc1, W_fc2) + b_fc2)

# the error between prediction and real 

cross_entropy = tf.reduce_mean(-tf.reduce_sum(ys*tf.log(prediction), reduce_indices = [1]))   #loss

train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)


sess = tf.Session()

sess.run(tf.initialize_all_variables())

for i in range(1000):
	# learning
	batch_xs, batch_ys = mnist.train.next_batch(100)       # using SGD  100 datas from data every step
	sess.run(train_step, feed_dict = {xs: batch_xs, ys: batch_ys})
	if i % 50 == 0:
		print(compute_accuracy(mnist.test.imgaes, mnist.test.labels))



		
		
		
		
		
		
		
		
		
		
		









































	

