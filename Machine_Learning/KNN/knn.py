# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import operator

def classify(inx, dataset, labels, k):
    datasetsize = dataset.shape[0]
    diffmat = np.tile(inx, (datasetsize, 1)) - dataset
    distance = ((diffmat**2).sum(axis=1))**0.5
    SortDistanceIndex = np.argsort(distance)
    classCount = {}
    for i in range(k):
        votelabel = labels[SortDistanceIndex[i]]
        classCount[votelabel] = classCount.get(votelabel,0) + 1
    sortedclasscount = sorted(classCount.iteritems(), key =operator.itemgetter(1),reverse=True )
    return sortedclasscount[0][0]


######First: define import data function##########

def file2matrix(filepath):
    path = open(filepath)                      # open(name[, mode[, buffering]]) output: object including close(), name(), reaslines()
    arrayolines = path.readlines()             # read the all rows from the input file
    numberlines = len(arrayolines)
    returnmat = np.zeros((numberlines,3))
    classlabelvector = []                      # the list can change or expand the size
    index = 0
    for line in arrayolines:
        line = line.strip()                    # remove the beginning and the end of input string
        listFromline = line.split('\t')
        returnmat[index,:] =listFromline[0:3]
        classlabelvector.append(listFromline[-1])
        index += 1
    return returnmat, classlabelvector

############ transfer the list label to label which denoted by '1','2','3'.###########

def typelabel2int(labelClass):
    res = {}                                           # define the dict
    for each in labelClass:
        res[each] = res.get(each, 0)+1
    names = res.keys()                                  # pick up the key of dict
    level = dict(zip(names, range(1,4,1)))
    for i in range(len(labelClass)):
        if labelClass[i] == names[0]:
            labelClass[i] = 1.
        if labelClass[i] == names[1]:
            labelClass[i] = 2.
        if labelClass[i] == names[2]:
            labelClass[i] = 3.
        labelClass = np.array(labelClass)

    return labelClass.astype(float),level

####################divide the dataset into initial classfication to plot#################
def cutdata(datamat, label):
    #datamat and label are all numpy.array
    datamatlist = datamat.tolist()
    labellist = label.tolist()
    datamat_c1 = []
    datamat_c2 = []
    datamat_c3 = []
    for index in range(len(labellist)):
        if labellist[index] == 1.0:
            datamat_c1.append(datamatlist[index])
        if labellist[index] == 2.0:
            datamat_c2.append(datamatlist[index])
        if labellist[index] == 3.0:
            datamat_c3.append(datamatlist[index])
    datamat_c1 = np.array(datamat_c1)
    datamat_c2 = np.array(datamat_c2)
    datamat_c3 = np.array(datamat_c3)
    return datamat_c1, datamat_c2, datamat_c3

datamat, labelClass = file2matrix(r'/home/python/venv/lib/RL_learning/statistic/chapter.1_KNN/datingTestSet.txt')
label,level = typelabel2int(labelClass)
datamat_c1, datamat_c2, datamat_c3 = cutdata(datamat, label)

##########plot the initial data############
fig1 = plt.figure(1)
fig1_1 = fig1.add_subplot(1, 2, 1)
plt.scatter(datamat[:,1],datamat[:,0],c='red')
plt.xlabel('feature2')
plt.ylabel('feature1')
plt.grid(True)
plt.title('Initial data show')

# color = 12*label , size = 15*label
# plt.scatter(datamat[:,1],datamat[:,0],s = 15.0*label, c = 16.0*label)
# plt.xlabel('feature2')
# plt.ylabel('feature1')
# plt.legend()
# plt.grid(True)
# plt.title('Labeled data show')
# fig1 = plt.figure(2)
fig1_2 = fig1.add_subplot(1, 2, 2)
plt.scatter(datamat_c1[:,1],datamat_c1[:,0],c='cyan',label = level.keys()[0])
plt.scatter(datamat_c2[:,1],datamat_c2[:,0],c='green',label = level.keys()[1])
plt.scatter(datamat_c3[:,1],datamat_c3[:,0],c='blue',label = level.keys()[2])
plt.xlabel('feature2')
plt.ylabel('feature1')
plt.grid(True)
plt.legend(loc = 0, ncol = 2)
plt.title('label data show')
plt.rcParams['figure.figsize'] = (8.0, 4.0)
fig1.savefig("initialdatashow.eps",dpi = 500)
print "datamat = ", '\n', datamat
################### normalize the matrix ########################
def autonormalize(datamat):
    for colum in range(len(datamat[0,:])):
        datamat[:,colum] = (datamat[:,colum] - np.min(datamat[:,colum]))/(np.max(datamat[:,colum])-np.min(datamat[:,colum]))
    return datamat
normailize_datamat = autonormalize(datamat)

hRatio = 0.1
num_total = normailize_datamat.shape[0]
num_test  =int(num_total * 0.1)
ecount = 0
preditive = np.zeros([num_test])
for i in range(num_test):
    preditive[i] = classify(normailize_datamat[i,:], normailize_datamat[num_test:num_total,:], label[num_test:num_total], 3)
    print "the predictive result is = ",preditive[i], "the real result is = ", label[i]
    if preditive[i] != label[i]:
        ecount += 1
rate = float(ecount)/float(num_test)
print "the error rate is ＝", rate*100,'%'
# print "normailize_datamat = ", '\n', normailize_datamat
# print "labels = ", '\n', label.shape
# print "classficationLevel" '\n' , level
plt.show()


