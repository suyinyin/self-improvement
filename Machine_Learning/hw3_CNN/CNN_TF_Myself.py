"""
please note that this code is only for python 3+. If you are using python2, please modify the python code accordingly

"""
#from __future__ import print_function  # import print function from new version
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data  # input the mnist data from tensorflow

mnist = input_data.read_data_sets('MNIST', one_hot=True)  # MNIST data sets


def compute_accuracy(v_vs, v_ys):
    global prediction
    y_pre = sess.run(prediction, feed_dict={xs: v_vs, keep_prob: 1})
    correct_prediction = tf.equal(tf.argmax(y_pre, 1), tf.argmax(v_ys, 1))
    # tf.argmax() find the index of max element along row:0 and column:1
    # tf.equal() return the comparative result of a and b, if a equals to b, return True otherwise return False. So its returns' type is Bool.
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    # tf.reduce_mean() compute the mean,
    # tf.cast(): convert the type of input variable to tf.float32
    result = sess.run(accuracy, feed_dict={xs: v_vs, ys: v_ys, keep_prob: 1})
    return result


# define weights and biases

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    # tf.truncated_normal(shape, mean, stddev) :shape is dimension
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def cov2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


# tf.nn.conv2d(input, filter, strides, padding, use_cudnn_on_gpu=None, name=None)
# input:[batch, in_height, in_width, in_channels] in_channel is the thickness
# filter:[filter_height, filter_width, in_channels, out_channels]
# stride [1, x_movement, y_movement, 1]
# padding is SAME or VALID
def max_poo_2x2(x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')


# tf.nn.max_pool(value, ksize, strides, padding, data_format='NHWC', name=None)
# value: [batch, height, width, channels]
# ksize[1, height, width, 1]
# stride [1, x_movement, y_movement, 1]
# padding is SAME or VALID

# define placeholder for input to networks

xs = tf.placeholder(tf.float32, [None, 784]) / 225.  # 28X28
ys = tf.placeholder(tf.float32, [None, 10])  # label
keep_prob = tf.placeholder(tf.float32)  # in case of over-fitting
x_image = tf.reshape(xs, [-1, 28, 28, 1])  # -1 denote we neglect the batch of samples

# conv1 layer

W_conv1 = weight_variable([5, 5, 1, 32])  # filter size 5X5 ,inchannel 1 and outchannel 32
b_conv1 = bias_variable([32])
h_conv1 = tf.nn.relu(cov2d(x_image, W_conv1) + b_conv1)  # outsize 28X28X32
h_pool1 = max_poo_2x2(h_conv1)  # output size 14x14x32

# conv2 layer

W_conv2 = weight_variable([5, 5, 32, 64])  # filter size 5X5 ,inchannel 32 and outchannel 64
b_conv2 = bias_variable([64])
h_conv2 = tf.nn.relu(cov2d(h_pool1, W_conv2) + b_conv2)  # outsize 14X14X32
h_pool2 = max_poo_2x2(h_conv2)  # outsize 7X7X64

# fully connected layer1

W_fc1 = weight_variable([7 * 7 * 64, 1024])  # go on oversize the nodes
b_fc1 = bias_variable([1024])
h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])  # [n_samples, 7, 7, 64] ->> [n_samples, 7*7*64]
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

# dropping function in case of over-fitting

h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)  # keep_prob in (0, 1.0) when it equals to 1 means choosing all data so dropout is useless

# fully connected layer2

W_fc2 = weight_variable([1024, 10])
b_fc2 = bias_variable([10])
prediction = tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)

# define the cost function

cross_entropy = tf.reduce_mean(-tf.reduce_sum(ys * tf.log(prediction), reduction_indices=[1]))

# do training
train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

sess = tf.Session()
init =tf.global_variables_initializer()
# or tf.global_variables_initializer() tf.initialize_all_variables()

sess.run(init)

for i in range(1000):
    batch_xs, batch_ys = mnist.train.next_batch(100)
    sess.run(train_step, feed_dict={xs: batch_xs, ys: batch_ys, keep_prob: 0.5})
    if i % 50 == 0:
        print(compute_accuracy(mnist.test.images[:1000], mnist.test.labels[:1000]))

