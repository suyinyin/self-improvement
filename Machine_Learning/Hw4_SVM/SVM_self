# -- coding: utf-8 --
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt




# import dataset including training data and testing data:
Trainingdata = pd.read_table('training_data.txt', header=None, sep='\s+')
Testingdata = pd.read_table('testing_data.txt',header=None,sep='\s+')
Trainingdata = Trainingdata.as_matrix()
Testingdata = Testingdata.as_matrix()
Trainingdata_positive = []                       # type list can be used to oversize the array
Trainingdata_negative = []
for i in np.arange(len(Trainingdata[:,0])):
    if Trainingdata[i ,2] == 1.0 :
        Trainingdata_positive.append([Trainingdata[i,0],Trainingdata[i,1],Trainingdata[i,2]])
    else:
        Trainingdata_negative.append([Trainingdata[i,0],Trainingdata[i,1],Trainingdata[i,2]])
Trainingdata_negative = np.array(Trainingdata_negative)
Trainingdata_positive = np.array(Trainingdata_positive)
plt.scatter(Trainingdata_positive[:, 0], Trainingdata_positive[:,1], c='red', marker='x', label='sample_positive')
plt.scatter(Trainingdata_negative[:, 0], Trainingdata_negative[:,1], c='blue', marker='x', label='sample_negative')

# choose j randomly which do not equal to i in m
def selectJrand(i, m):
    j = i
    while j == i:
        j = int(np.random.uniform(0, m))
    return j

# make sure L <= aj <= H
def clipAlpha(aj,H,L):
    if aj > H:
        aj = H
    if aj < L:
        aj = L
    return aj

#define kernel function
def kernel(X, A, kTup):  # 核函数，输入参数,X:支持向量的特征树；A：某一行特征数据；kTup：('lin',k1)核函数的类型和参数
    m, n = X.shape
    K = np.mat(np.zeros((m, 1)))
    if kTup[0] == 'lin':  # linear function
        K = X * A.T
    elif kTup[0] == 'rbf':  # radial bias function(Gaussian)
        for j in range(m):
            deltaRow = X[j, :] - A
            K[j] = deltaRow * deltaRow.T
        K = np.exp(K / (-1 * kTup[1] ** 2))
    else:
        raise NameError('Houston We Have a Problem -- That Kernel is not recognized')
    return K


# define class to store data
class optStruct:
    def __init__(self,dataMatIn, classLabels, C, toler, kTup):  # 存储各类参数
        self.X = dataMatIn  #数据特征
        self.labelMat = classLabels #数据类别
        self.C = C #软间隔参数C，参数越大，非线性拟合能力越强
        self.tol = toler #停止阀值
        self.m = dataMatIn.shape[0] #数据行数
        self.alphas = np.mat(np.zeros((self.m,1)))
        self.b = 0 #初始设为0
        self.eCache = np.mat(np.zeros((self.m,2))) #缓存
        self.K = np.mat(np.zeros((self.m,self.m))) #核函数的计算结果
        for i in range(self.m):
            self.K[:,i] = kernel(self.X, self.X[i,:], kTup)


# compute error
def calcEk(oS, k): #计算Ek（参考《统计学习方法》p127公式7.105）
    fXk = float(np.multiply(oS.alphas,oS.labelMat).T*oS.K[:,k] + oS.b)
    Ek = fXk - float(oS.labelMat[k])
    return Ek

def selectJ(i, oS, Ei):
    maxK = -1
    maxDeltaE = 0
    Ej = 0
    oS.eCache[i] = [1,Ei]
    validEcacheList = np.nonzero(oS.eCache[:,0].A)[0]  #返回矩阵中的非零位置的行数
    if (len(validEcacheList)) > 1:
        for k in validEcacheList:
            if k == i:
                continue
            Ek = calcEk(oS, k)
            deltaE = abs(Ei - Ek)
            if (deltaE > maxDeltaE): #返回步长最大的aj
                maxK = k
                maxDeltaE = deltaE
                Ej = Ek
        return maxK, Ej
    else:
        j = selectJrand(i, oS.m)
        Ej = calcEk(oS, j)
    return j, Ej

def updateEk(oS, k): #更新os数据
    Ek = calcEk(oS, k)
    oS.eCache[k] = [1,Ek]


#首先检验ai是否满足KKT条件，如果不满足，随机选择aj进行优化，更新ai,aj,b值
def innerL(i, oS): #输入参数i和所有参数数据
    Ei = calcEk(oS, i) #计算E值
    if ((oS.labelMat[i]*Ei < -oS.tol) and (oS.alphas[i] < oS.C)) or ((oS.labelMat[i]*Ei > oS.tol) and (oS.alphas[i] > 0)): #检验这行数据是否符合KKT条件 参考《统计学习方法》p128公式7.111-113
        j,Ej = selectJ(i, oS, Ei) #随机选取aj，并返回其E值
        alphaIold = oS.alphas[i].copy()
        alphaJold = oS.alphas[j].copy()
        if (oS.labelMat[i] != oS.labelMat[j]): #以下代码的公式参考《统计学习方法》p126
            L = max(0, oS.alphas[j] - oS.alphas[i])
            H = min(oS.C, oS.C + oS.alphas[j] - oS.alphas[i])
        else:
            L = max(0, oS.alphas[j] + oS.alphas[i] - oS.C)
            H = min(oS.C, oS.alphas[j] + oS.alphas[i])
        if L==H:
            print("L==H")
            return 0
        eta = 2.0 * oS.K[i,j] - oS.K[i,i] - oS.K[j,j] #参考《统计学习方法》p127公式7.107
        if eta >= 0:
            print("eta>=0")
            return 0
        oS.alphas[j] -= oS.labelMat[j]*(Ei - Ej)/eta #参考《统计学习方法》p127公式7.106
        oS.alphas[j] = clipAlpha(oS.alphas[j],H,L) #参考《统计学习方法》p127公式7.108
        updateEk(oS, j)
        if (abs(oS.alphas[j] - alphaJold) < oS.tol): #alpha变化大小阀值（自己设定）
            print("j not moving enough")
            return 0
        oS.alphas[i] += oS.labelMat[j]*oS.labelMat[i]*(alphaJold - oS.alphas[j])#参考《统计学习方法》p127公式7.109
        updateEk(oS, i) #更新数据
        b1 = oS.b - Ei - oS.labelMat[i] * (oS.alphas[i] - alphaIold) * oS.K[i, i] - oS.labelMat[j] * (
                oS.alphas[j] - alphaJold) * oS.K[i, j]
        b2 = oS.b - Ej - oS.labelMat[i] * (oS.alphas[i] - alphaIold) * oS.K[i, j] - oS.labelMat[j] * (
                oS.alphas[j] - alphaJold) * oS.K[j, j]
        if (0 < oS.alphas[i] < oS.C):
            oS.b = b1
        elif (0 < oS.alphas[j] < oS.C):
            oS.b = b2
        else:
            oS.b = (b1 + b2) / 2.0
        return 1
    else:
        return 0

#SMO函数，用于快速求解出alpha
def smoP(dataMatIn, classLabels, C, toler, maxIter,kTup=('lin', 0)): #输入参数：数据特征，数据类别，参数C，阀值toler，最大迭代次数，核函数（默认线性核）
    oS = optStruct(np.mat(dataMatIn),np.mat(classLabels).T,C,toler, kTup)
    iter = 0
    entireSet = True
    alphaPairsChanged = 0
    while (iter < maxIter) and ((alphaPairsChanged > 0) or (entireSet)):
        alphaPairsChanged = 0
        if entireSet:
            for i in range(oS.m): #遍历所有数据
                alphaPairsChanged += innerL(i,oS)
                print("fullSet, iter: %d i:%d, pairs changed %d" % (iter,i,alphaPairsChanged)) #显示第多少次迭代，那行特征数据使alpha发生了改变，这次改变了多少次alpha
            iter += 1
        else:
            nonBoundIs = np.nonzero((oS.alphas.A > 0) * (oS.alphas.A < C))[0]
            for i in nonBoundIs: #遍历非边界的数据
                alphaPairsChanged += innerL(i,oS)
                print("non-bound, iter: %d i:%d, pairs changed %d" % (iter,i,alphaPairsChanged))
            iter += 1
        if entireSet:
            entireSet = False
        elif (alphaPairsChanged == 0):
            entireSet = True
        print("iteration number: %d" % iter)
    return oS.b,oS.alphas

def testRbf(data_train,data_test):
    # dataArr,labelArr = loadDataSet(data_train) #读取训练数据
    dataArr = data_train[:,:2]
    labelArr = data_train[:,2]
    b,alphas = smoP(data_train[:,:2], data_train[:,2], 200, 0.0001, 10000, ('rbf', 1.3)) #通过SMO算法得到b和alpha
    datMat=np.mat(dataArr)
    labelMat = np.mat(labelArr).T
    svInd=np.nonzero(alphas)[0]  #选取不为0数据的行数（也就是支持向量）
    sVs=datMat[svInd] #支持向量的特征数据
    labelSV = labelMat[svInd] #支持向量的类别（1或-1）
    print("there are %d Support Vectors" % np.shape(sVs)[0]) #打印出共有多少的支持向量
    m,n = np.shape(datMat) #训练数据的行列数
    errorCount = 0
    for i in range(m):
        kernelEval = kernel(sVs,datMat[i,:],('rbf', 1.3)) #将支持向量转化为核函数
        predict=kernelEval.T * np.multiply(labelSV,alphas[svInd]) + b  #这一行的预测结果（代码来源于《统计学习方法》p133里面最后用于预测的公式）注意最后确定的分离平面只有那些支持向量决定。
        if np.sign(predict)!=np.sign(labelArr[i]): #sign函数 -1 if x < 0, 0 if x==0, 1 if x > 0
            errorCount += 1
    print("the training error rate is: %f" % (float(errorCount)/m)) #打印出错误率
    # dataArr_test,labelArr_test = loadDataSet(data_test) #读取测试数据
    dataArr_test = data_test[:,:2]
    labelArr_test = data_test[:, 2]
    errorCount_test = 0
    datMat_test=np.mat(dataArr_test)
    labelMat = np.mat(labelArr_test).transpose()
    m,n = np.shape(datMat_test)
    for i in range(m): #在测试数据上检验错误率
        kernelEval = kernel(sVs,datMat_test[i,:],('rbf', 1.3))
        predict=kernelEval.T *np.multiply(labelSV,alphas[svInd]) + b
        if np.sign(predict)!=np.sign(labelArr_test[i]):
            errorCount_test += 1
    print("the test error rate is: %f" % (float(errorCount_test)/m))

testRbf(Trainingdata, Testingdata)



plt.show()
