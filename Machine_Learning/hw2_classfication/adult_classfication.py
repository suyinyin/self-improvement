import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import sklearn as skl
import sklearn.preprocessing as preprocessing
import sklearn.linear_model as linear_model
import sklearn.metrics as metrics
import sklearn.tree as tree
import seaborn as sns



# import adult data set
names = ['Age', 'Workclass', 'fnlwgt', 'Education', 'Education-Num', 'Martial_Status', 'Occupation',
         'Relationship', 'Race', 'Sex', 'Capital_Gain', 'Capital_Loss', 'Hours_Per_Week', 'Country', 'Target']

Adult_data = pd.read_csv('adult.data.txt', names = names, sep = ',', na_values = 'NAN')
# print(Adult_data['Education'].value_counts())
# print(Adult_data['Education-Num'].value_counts())

#do classfication training logistic regression

def LR_Classfication(dataset, learningrate):
    features = dataset
    weights = np.zeros(dataset.shape[1]-1)
    weights1 = np.zeros(dataset.shape[1]-1)
    biase = 0
    j = 0
    while j < 5000:
        y_prediction_transfer = (np.dot(weights, features[:,:(dataset.shape[1]-1)].T) + biase)   # n X 1
        y_prediction = 1.0 / (1 + np.exp(-y_prediction_transfer))
        for i in np.arange(len(features[:, 0])):
            for jj in np.arange(len(weights)):
                weights1[jj] = weights[jj] + learningrate *((features[i, dataset.shape[1]-1] - y_prediction[i]) * features[i, jj])
            biase1 = biase + learningrate * (features[i, dataset.shape[1]-1] - y_prediction[i]) * 1
            weights = weights1
            biase = biase1
        j += 1
    return weights, biase

# encode categorical features as number
def encode_categoricalfeature(dataset):
    for i, column in enumerate(dataset.columns):
        if dataset.dtypes[column] == np.object:
            transfer = dataset[column].unique()                         # unique() denote compute the category of one column
            mapping = dict(zip(transfer, np.arange(len(transfer))))                  # dict(zip(['one', 'two', 'three'], [1, 2, 3]))
            dataset[column] = dataset[column].map(mapping)
            del transfer
    return dataset

def number_encode_features(df):
    result = df.copy()
    encoders = {}
    for column in result.columns:
        if result.dtypes[column] == np.object:
            encoders[column] = preprocessing.LabelEncoder()
            result[column] = encoders[column].fit_transform(result[column])
    return result
Adult_data_number = number_encode_features(Adult_data)
del Adult_data_number['Education']                               # because of the strong correlation between Education and Education-Num, we delete the the Education column
print(Adult_data_number.tail(20))
Adult_data_number1 = Adult_data_number.values

fig = plt.figure(figsize=(20, 15),num = 1)    # generate a figure and we can produce the second figure by 'plt.figure(figsize=(20, 15), num = 2 or 'my')'
cols = 5
rows = math.ceil(float(Adult_data.shape[1]) / cols)   # .shape[1] means the column length of Adult_data. ceil(5.6) denotes we will obtain 6 after using ceil function.
for i, column in enumerate(Adult_data.columns):       # i : index and columm : column names
    ax = fig.add_subplot(rows, cols, i + 1)
    ax.set_title(column)
    if Adult_data.dtypes[column] == np.object:
        Adult_data[column].value_counts().plot(kind="bar", axes=ax)  #value_counts() computes frequency of one category occuring in one column
    else:
        Adult_data[column].hist(axes=ax)   #  axes and subplot are nearly the same, the different between axes and subplot
                                           #  is axes(0, 0 , 8 , 10) could give the scope or range of the figure, while scope of subplot is default.
        plt.xticks(rotation="vertical")
plt.subplots_adjust(hspace=0.7, wspace=0.2)
# weights, biase = LR_Classfication(Adult_data_number1, 0.0001)

fig = plt.figure(figsize=(20, 15),num = 2)
sns.heatmap(Adult_data_number.corr(), square=True)
plt.show()
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
